insert into medical_condition(medcon_name, medcon_descr) values
('Diabetes', 'Diabetes is a number of diseases that involve problems with the hormone insulin'),
('High Blood Pressure', 'High blood pressure is a common condition in which the long-term force of the blood against your artery walls is high enough that it may eventually cause health problems, such as heart disease'),
('Low Blood Pressure', 'A blood pressure reading lower than 90 millimeters of mercury (mm Hg) for the top number (systolic) or 60 mm Hg for the bottom number (diastolic) is generally considered low blood pressure');