
insert into staff_category (job_title, base_salary) values ('Accountant', 53540);
insert into staff_category (job_title, base_salary) values ('Medical Officer', 84400);
insert into staff_category (job_title, base_salary) values ('Nurse', 21000);
insert into staff_category (job_title, base_salary) values ('Lab Supervisor', 40000);
insert into staff_category (job_title, base_salary) values ('Blood Technician', 25000);
insert into staff_category (job_title, base_salary) values ('Manager', 56000);
insert into staff_category (job_title, base_salary) values ('Receptionist', 30000);