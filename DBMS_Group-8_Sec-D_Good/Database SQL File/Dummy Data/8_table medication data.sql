insert into medication(med_name, med_descr, med_guide) values
('Afrezza', 'A rapid-acting inhaled insulin', 'Use before meals for both type 1 and type 2 diabetes'),
('Acebutolol', 'Acebutolol is a beta blocker for the treatment of hypertension and arrhythmias', '1+0+1'),
('Midodrine (Orvaten)', 'Raises standing blood pressure levels in people with chronic orthostatic hypotension', '1+0+1');