1. Open MySQL Workbench

2. Create a database named 'blood_bank'

3. Click File -> Open SQL Script
	
	Choose CREATE TABLE.sql and press open

4. Execute the file

5. If you want to add dummy data to test the application
	
	- Click File -> Open SQL Script
	
	- Go to Database SQL File\Dummy Data
		
		- Open and execute files one by one sequentially