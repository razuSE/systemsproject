CREATE TABLE address(
	add_id INT(6) NOT NULL AUTO_INCREMENT,
	house_no VARCHAR(10),
	street VARCHAR(255),
	area VARCHAR(255),
	zip_code VARCHAR(16),
	city VARCHAR(255),
	country VARCHAR(74),
	CONSTRAINT address_pk PRIMARY KEY(add_id)
);

ALTER TABLE address AUTO_INCREMENT=100000;

CREATE TABLE person(
	per_id INT(6) NOT NULL AUTO_INCREMENT,
	add_id INT(6),
	f_name VARCHAR(30),
	l_name VARCHAR(30),
	dob DATE,
	mobile_no VARCHAR(20),
	sex VARCHAR(6),
	pic BLOB,
	email VARCHAR(254),
	CONSTRAINT person_pk PRIMARY KEY(per_id),
	CONSTRAINT person_address_fk FOREIGN KEY(add_id) REFERENCES address(add_id) ON UPDATE CASCADE ON DELETE SET NULL
);

ALTER TABLE person AUTO_INCREMENT=100000;

CREATE TABLE user(
	u_id INT(6) NOT NULL AUTO_INCREMENT,
	per_id INT(6) UNIQUE,
	u_name VARCHAR(255) UNIQUE,
	pass VARCHAR(255),
	status VARCHAR(10) DEFAULT 'Pending',
    access_level VARCHAR(30) DEFAULT 'record manager',
	CONSTRAINT user_pk PRIMARY KEY(u_id),
	CONSTRAINT user_person_fk FOREIGN KEY(per_id) REFERENCES person(per_id) ON UPDATE CASCADE ON DELETE SET NULL
);

ALTER TABLE user AUTO_INCREMENT=100000;

CREATE TABLE staff_category(
	cat_id INT(6) NOT NULL AUTO_INCREMENT,
	job_title VARCHAR(255),
	job_descr TEXT,
	edu_req TEXT,
	exp_req TEXT,
	base_salary INTEGER(7),
	CONSTRAINT staff_cat_pk PRIMARY KEY(cat_id)
);

ALTER TABLE staff_category AUTO_INCREMENT=100000;

CREATE TABLE staff(
	staff_id INT(6) NOT NULL AUTO_INCREMENT,
    per_id INT(6) UNIQUE,
    cat_id INT(6),
    edu_back TEXT,
    exp_details TEXT,
    ref_details TEXT,
    join_date DATE,
    resign_date DATE,
    CONSTRAINT staff_pk PRIMARY KEY(staff_id),
    CONSTRAINT staff_person_fk FOREIGN KEY(per_id) REFERENCES person(per_id) ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT staff_staff_cat_fk FOREIGN KEY(cat_id) REFERENCES staff_category(cat_id) ON UPDATE CASCADE ON DELETE SET NULL
);

ALTER TABLE staff AUTO_INCREMENT=100000;

CREATE TABLE donor(
	dnr_id INT(6) NOT NULL AUTO_INCREMENT,
    per_id INT(6) UNIQUE,
    bld_group VARCHAR(3),
    CONSTRAINT donor_pk PRIMARY KEY(dnr_id),
    CONSTRAINT donor_person_fk FOREIGN KEY(per_id) REFERENCES person(per_id) ON UPDATE CASCADE ON DELETE SET NULL
);

ALTER TABLE donor AUTO_INCREMENT=100000;

CREATE TABLE medication(
	med_id INT(6) NOT NULL AUTO_INCREMENT,
    med_name VARCHAR(255),
    med_descr TEXT,
    med_guide TEXT,
    CONSTRAINT medication_pk PRIMARY KEY(med_id)
);

ALTER TABLE medication AUTO_INCREMENT=100000;

CREATE TABLE dnr_medication(
	dnr_id INT(6) NOT NULL,
    med_id INT(6) NOT NULL,
    CONSTRAINT dnr_medication_pk PRIMARY KEY(dnr_id, med_id),
    CONSTRAINT dnr_medication_fk1 FOREIGN KEY(dnr_id) REFERENCES donor(dnr_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT dnr_medication_fk2 FOREIGN KEY(med_id) REFERENCES medication(med_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE medical_condition(
	medcon_id INT(6) NOT NULL AUTO_INCREMENT,
    medcon_name VARCHAR(255),
    medcon_descr TEXT,
    CONSTRAINT medical_con_pk PRIMARY KEY(medcon_id)
);

ALTER TABLE medical_condition AUTO_INCREMENT=100000;

CREATE TABLE dnr_medcon(
	dnr_id INT(6) NOT NULL,
    medcon_id INT(6) NOT NULL,
    severity VARCHAR(255),
    CONSTRAINT dnr_medcon_pk PRIMARY KEY(dnr_id, medcon_id),
    CONSTRAINT dnr_medcond_fk1 FOREIGN KEY(dnr_id) REFERENCES donor(dnr_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT dnr_medcond_fk2 FOREIGN KEY(medcon_id) REFERENCES medical_condition(medcon_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE donation(
	don_id INT(6) NOT NULL AUTO_INCREMENT,
    dnr_id INT(6),
    don_date DATE,
    expiry_date DATE,
    dispatched VARCHAR(3),
    CONSTRAINT donation_pk PRIMARY KEY(don_id),
    CONSTRAINT donation_donor_fk FOREIGN KEY(dnr_id) REFERENCES donor(dnr_id) ON UPDATE CASCADE ON DELETE SET NULL
);

ALTER TABLE donation AUTO_INCREMENT=100000;

CREATE TABLE blood_receiver(
	rec_id INT(6) NOT NULL AUTO_INCREMENT,
    don_id INT(6) UNIQUE,
    per_id INT(6),
    rec_date DATE,
    CONSTRAINT blood_receiver_pk PRIMARY KEY(rec_id),
    CONSTRAINT blood_rec_don_fk FOREIGN KEY(don_id) REFERENCES donation(don_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT blood_rec_per_fk FOREIGN KEY(per_id) REFERENCES person(per_id) ON UPDATE CASCADE ON DELETE SET NULL
);

ALTER TABLE blood_receiver AUTO_INCREMENT=100000;