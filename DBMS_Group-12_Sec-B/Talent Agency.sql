-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: t_agency
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `addresses` (
  `Address_Id` int(11) NOT NULL,
  `Address_Details` varchar(255) NOT NULL,
  PRIMARY KEY (`Address_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `agents` (
  `Agent_Id` int(11) NOT NULL,
  `Address_Id` int(11) NOT NULL,
  `Agent_Details` varchar(255) NOT NULL,
  PRIMARY KEY (`Agent_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents`
--

LOCK TABLES `agents` WRITE;
/*!40000 ALTER TABLE `agents` DISABLE KEYS */;
/*!40000 ALTER TABLE `agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditions`
--

DROP TABLE IF EXISTS `auditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auditions` (
  `Audition_Id` int(11) NOT NULL,
  `Address_Id` int(11) NOT NULL,
  `Audition_Outcome_Code` char(15) NOT NULL,
  `Client_Id` int(11) NOT NULL,
  `Talent_Id` int(11) NOT NULL,
  `Audition_Date_Time` datetime NOT NULL,
  `Audition_Location` varchar(255) NOT NULL,
  `Contact_Name` varchar(255) NOT NULL,
  `Other_Details` varchar(255) NOT NULL,
  PRIMARY KEY (`Audition_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditions`
--

LOCK TABLES `auditions` WRITE;
/*!40000 ALTER TABLE `auditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `clients` (
  `Clients_Id` int(11) NOT NULL,
  `Address_Id` int(11) NOT NULL,
  `Client_Name` varchar(255) NOT NULL,
  `Date_become_Client` datetime NOT NULL,
  `Date_of_Last_Contact` datetime NOT NULL,
  `Contact_First_Name` varchar(255) NOT NULL,
  `Contact_Last_Name` varchar(255) NOT NULL,
  `Other_Details` varchar(255) NOT NULL,
  PRIMARY KEY (`Clients_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (10,10,'moinul hassan siddiquie','2025-11-18 00:00:00','2029-11-18 00:00:00','jony','siddiquie','He is a good Engineer');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performances`
--

DROP TABLE IF EXISTS `performances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `performances` (
  `Performance_Id` int(11) NOT NULL,
  `Address_Id` int(11) NOT NULL,
  `Audition_Id` int(11) NOT NULL,
  `Assignment_Start_Date` datetime NOT NULL,
  `Assignment_End_Date` datetime NOT NULL,
  `Assignment_Location` varchar(255) NOT NULL,
  `Contact_Name` varchar(255) NOT NULL,
  `Other_Details` varchar(255) NOT NULL,
  PRIMARY KEY (`Performance_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performances`
--

LOCK TABLES `performances` WRITE;
/*!40000 ALTER TABLE `performances` DISABLE KEYS */;
/*!40000 ALTER TABLE `performances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performers`
--

DROP TABLE IF EXISTS `performers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `performers` (
  `Performer_Id` int(11) NOT NULL,
  `Address_Id` int(11) NOT NULL,
  `Agent_Id` int(11) NOT NULL,
  `Ethnic_Origin_Code` char(15) NOT NULL,
  `Talent_Agency_Id` int(11) NOT NULL,
  `Stage_Name` varchar(255) NOT NULL,
  `First_Name` varchar(255) NOT NULL,
  `Middle_Name` varchar(255) NOT NULL,
  `Last_Name` varchar(255) NOT NULL,
  `Date_Of_Birth` datetime NOT NULL,
  `Gender_MFU` char(1) NOT NULL,
  `Profile` varchar(255) NOT NULL,
  `Other_Details` varchar(255) NOT NULL,
  PRIMARY KEY (`Performer_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performers`
--

LOCK TABLES `performers` WRITE;
/*!40000 ALTER TABLE `performers` DISABLE KEYS */;
/*!40000 ALTER TABLE `performers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performers_skills`
--

DROP TABLE IF EXISTS `performers_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `performers_skills` (
  `Performer_Id` int(11) NOT NULL,
  `Skill_Code` char(15) NOT NULL,
  PRIMARY KEY (`Skill_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performers_skills`
--

LOCK TABLES `performers_skills` WRITE;
/*!40000 ALTER TABLE `performers_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `performers_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_audition_outcomes`
--

DROP TABLE IF EXISTS `ref_audition_outcomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ref_audition_outcomes` (
  `Audition_Outcome_Code` char(15) NOT NULL,
  `Audition_Outcome_Description` varchar(255) NOT NULL,
  PRIMARY KEY (`Audition_Outcome_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_audition_outcomes`
--

LOCK TABLES `ref_audition_outcomes` WRITE;
/*!40000 ALTER TABLE `ref_audition_outcomes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ref_audition_outcomes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_ethnic_origins`
--

DROP TABLE IF EXISTS `ref_ethnic_origins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ref_ethnic_origins` (
  `Ethnic_Origin_Code` char(15) NOT NULL,
  `Ethnic_Origin_Description` varchar(255) NOT NULL,
  PRIMARY KEY (`Ethnic_Origin_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_ethnic_origins`
--

LOCK TABLES `ref_ethnic_origins` WRITE;
/*!40000 ALTER TABLE `ref_ethnic_origins` DISABLE KEYS */;
/*!40000 ALTER TABLE `ref_ethnic_origins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_skills`
--

DROP TABLE IF EXISTS `ref_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ref_skills` (
  `Skill_Code` int(11) NOT NULL,
  `Skill_Description` varchar(255) NOT NULL,
  `eg Actor,Dancer` char(1) NOT NULL,
  `eg pianist,singer` char(1) NOT NULL,
  PRIMARY KEY (`Skill_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_skills`
--

LOCK TABLES `ref_skills` WRITE;
/*!40000 ALTER TABLE `ref_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `ref_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_agency`
--

DROP TABLE IF EXISTS `t_agency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `t_agency` (
  `id` int(11) NOT NULL,
  `address id` varchar(45) NOT NULL,
  `details` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_agency`
--

LOCK TABLES `t_agency` WRITE;
/*!40000 ALTER TABLE `t_agency` DISABLE KEYS */;
INSERT INTO `t_agency` VALUES (1,'3','i am a good boy'),(2,'4','she is a cute girl'),(3,'5','He is a good deiver'),(4,'6','abrar is a honest person');
/*!40000 ALTER TABLE `t_agency` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-07 22:41:42
